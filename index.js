// statement
console.log("Hello World!") 

// syntax e.g
// console
// log()


// camelCasing firstName
// snake casing first_name
// kebab casing first-name 
// pascal casing FirstName 

// VARIABLES
let productName = 'desktop computer';
let productPrice = 18999;
const PI = 3.1416;
// above code storing values

// let - variables are variables CAN be re-assigned
let firstName = 'chen';
console.log(firstName);

let lastName = 'Aradanas';
console.log('Baguio ' + lastName);

// re-assigning the variable, let-key word not needed anymore
firstName = 'Mich';
console.log(firstName);


// const variables are fixed
const colorOfTheSun = 'yellow';
console.log('The color of the sun is ' + colorOfTheSun );

// tried to change the value "yellow to red" but error occur since const is fixed variables.
// colorOfTheSun = 'red'
// console.log(colorOfTheSun);

// DECLARATION
let variableName = 'value';

// DATA types

// 1. String
let personName = 'New Value';

// 2. Number
let personAge = '5';

// 3. Boolean
let hasGirlFriend = 'false';

// 4. Array
let hobbies = ['cycling, gym, trekking'];
// array - values only
// To call the specific value inside of an array.
console.log(hobbies[0])

// 5. objects
// names + value
let person = {
    personName: 'chen',
    personAge: '2',
    hasGirlFriend: 'false',
    hobbies: ['cyling, gym, trekking'],
}
// To display the value of an object
console.log(person)
// To call value inside curly brackets of an object
console.log(person.personAge)

// 6. NUll
let wallet = 'null';


// e.g of null by ernest
let name = null;
let id = 1;

if (id == 1) {
    name = "user1";
} else {
    name = "user2";
}

if (name == null) {

}

console.log("name is : " + name);






 
 
